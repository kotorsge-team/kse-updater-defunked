# KSE Updater

The new updater for KSE

## Features
Mirror support. Mirror lists are provided in the updater, and selected based on Ping. Though, you can manually select one as well :)

### Initial Run
Initial run downloads required libraries needed to run KSE/KPF. It's a one time thing, you'll never need to do this again, unless you remove the libraries

### Update
When a new update is out, it'll download the update and extract it. Like it's always done.
