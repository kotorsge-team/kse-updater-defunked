﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;

namespace kse_updater
{
	public class UpdateCheck
	{
		private string updateServer = "http://cdn.kalebklein.com/kse/";
		public UpdateInfo JSON { get; private set; }
		private string script;

		public UpdateCheck(string UpdateScript)
		{
			script = UpdateScript;
		}

		public bool check()
		{
			try
			{
				var req = HttpWebRequest.Create(updateServer + script);
				var res = req.GetResponse();

				using (var reader = new StreamReader(res.GetResponseStream()))
				{
					JSON = new JavaScriptSerializer().Deserialize<UpdateInfo>(reader.ReadToEnd());
				}

				return true;
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.ToString());
				return false;
			}
		}
	}
}
