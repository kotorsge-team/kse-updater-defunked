﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace kse_updater
{
	public class CommandLine
	{
		private string[] cmdArgs;
		/// <summary>
		/// This class only parses command line arguments :)
		/// </summary>
		public CommandLine()
		{
			cmdArgs = Environment.GetCommandLineArgs();
		}

//another comment

		public void parse()
		{
			for (int i = 0; i < cmdArgs.Length; i++)
			{
				if(cmdArgs[i].StartsWith("-"))
				{
					if (cmdArgs[i].Equals("-v"))
						App.Version = cmdArgs[i + 1];

					if (cmdArgs[i].Equals("-vc"))
						App.VersionCode = int.Parse(cmdArgs[i + 1]);

					if (cmdArgs[i].Equals("-ir"))
						App.InitialRun = bool.Parse(cmdArgs[i + 1]);
				}
			}
		}
	}
}
