﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace kse_updater
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		public static string		Version			{ get; set; }
		public static int			VersionCode		{ get; set; }
		public static bool			InitialRun		{ get; set; }
		public static UpdateInfo	UInfo			{ get; private set; }
		public static Process		ParentProcess	{ get; private set; }

		private bool isUpdate = false;

		public App()
		{
			var cmd = new CommandLine();
			cmd.parse();
			ParentProcess = KSEProcess.GetParentProcess();

			// Check for an internet connection
			var sc = new StatusCheck("http://cdn.kalebklein.com/kse/testcl.txt");
			if(sc.IsConnected)
			{
				var updater = new UpdateCheck("update.json");
				if(updater.check())
				{
					//MirrorList = updater.JSON.mirrors; // Set mirror list
					UInfo = updater.JSON;
					if(int.Parse(updater.JSON.versionCode) > VersionCode)
					{
						new MainWindow().Show();
						isUpdate = true;
					}
					else
					{
						if (InitialRun)
							new MainWindow().Show();
					}
				}
				else
				{
					MessageBox.Show("You are already up-to-date!");
					App.Destroy();
				}
			}
			else
			{
				MessageBox.Show("You are already up-to-date!");
				App.Destroy();
			}

			if (!InitialRun && !isUpdate)
				App.Destroy();
		}

		public static void Destroy()
		{
			App.Current.Shutdown();
		}
	}
}
