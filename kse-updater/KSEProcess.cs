﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace kse_updater
{
	public static class KSEProcess
	{
		/// <summary>
		/// Get the parent process by a known ID
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public static Process GetParentProcessById(int id)
		{
			var query = string.Format("SELECT ParentProcessID FROM Win32_Process WHERE ProcessID = {0}", id);
			var search = new ManagementObjectSearcher("root\\CIMV2", query);
			var results = search.Get().GetEnumerator();
			results.MoveNext();
			var queryObj = results.Current;
			var parentId = (uint)queryObj["ParentProcessId"];
			var parent = Process.GetProcessById((int)parentId);

			return parent;
		}

		/// <summary>
		/// Get the parent process of the current running process
		/// </summary>
		/// <returns></returns>
		public static Process GetParentProcess()
		{
			var myId = Process.GetCurrentProcess().Id;
			var parent = GetParentProcessById((int)myId);

			return parent;
		}
	}
}
