﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kse_updater
{
	public class UpdateInfo
	{
		public string version { get; set; }
		public string versionCode { get; set; }
		public string libDist { get; set; }
		public string updateDist { get; set; }
		public List<string> mirrors { get; set; }
	}
}
