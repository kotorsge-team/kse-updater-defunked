﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace kse_updater
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private string selectedMirror;
		private string TempDir;
		private string zipfile;
		private WebClient client;

		public MainWindow()
		{
			InitializeComponent();

			TempDir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Temp\\";

			if (App.InitialRun)
			{
				zipfile = TempDir + "libs.zip";
				browserChangelog.Navigate("http://cdn.kalebklein.com/kse/initialRun.html");
			}
			else
			{
				zipfile = TempDir + "update.zip";
				browserChangelog.Navigate("http://cdn.kalebklein.com/kse/changes.html");
			}

			populateMirrorList();
		}

		private void populateMirrorList()
		{
			// Remove extras from mirrors. They don't need to see it
			var list = App.UInfo.mirrors;
			var itemsList = new List<string>();
			foreach(var item in list)
			{
				var i = item.Split('/');

				itemsList.Add(i[2]);
			}
			cmbMirrorList.ItemsSource = itemsList;
			
			Ping p = new Ping();
			PingReply pr;
			long lastPingTime = 0;
			int selectedIndex = 0;

			for (int i = 0; i < itemsList.Count; i++)
			{
				pr = p.Send(itemsList[i]);
				if(pr.Status == IPStatus.Success)
				{
					if (((pr.RoundtripTime <= lastPingTime) && (lastPingTime != 0)) 
						|| itemsList.Count == 1)
					{
						selectedIndex = i;
					}
					itemsList[i] = itemsList[i] + " // Ping: -" + pr.RoundtripTime + "ms";
					lastPingTime = pr.RoundtripTime;
					cmbMirrorList.SelectedIndex = selectedIndex;
				}
				else
				{
					itemsList[i] = itemsList[i] + " - No Connection";
				}
			}
		}

		private void bCancel_Click(object sender, RoutedEventArgs e)
		{
			if(!App.InitialRun)
			{
				ProcessStartInfo psi = new ProcessStartInfo();
				psi.WorkingDirectory = Environment.CurrentDirectory;
				psi.Arguments = "--dontRerun";
				psi.FileName = "kse.exe";
				Process.Start(psi);
			}

			this.Close();
		}

		private void bUpdate_Click(object sender, RoutedEventArgs e)
		{
			string selItem = string.Empty;
			foreach(var item in App.UInfo.mirrors)
			{
				if(item.Contains(cmbMirrorList.SelectedValue.ToString().Split(null)[0]))
				{
					selectedMirror = item;
				}
			}

			string distFile;
			if (App.InitialRun)
				distFile = App.UInfo.libDist;
			else
				distFile = App.UInfo.updateDist;

			try
			{
				if (!App.InitialRun)
				{
					Process p = Process.GetProcessById(App.ParentProcess.Id);
					p.Kill();
				}
			}
			catch { }

			client = new WebClient();
			client.DownloadProgressChanged += client_DownloadProgressChanged;
			client.DownloadFileCompleted += client_DownloadFileCompleted;
			client.DownloadFileAsync(new Uri(selectedMirror + distFile), zipfile);
			bUpdate.IsEnabled = false;
			bCancel.IsEnabled = false;
		}

		private void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
		{
			double bytesIn = double.Parse(e.BytesReceived.ToString());
			double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
			double percent = bytesIn / totalBytes * 100;
			pbProgress.Value = int.Parse(Math.Truncate(percent).ToString());

			statusText.Text = "Downloading: " + pbProgress.Value.ToString() + "% // " + bytesIn + "/" + totalBytes;
		}

		private void client_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
		{
			pbProgress.Value = 100;
			pbProgress.IsIndeterminate = true;
			statusText.Text = "Extracting update...";

			if (!Directory.Exists("platforms"))
				Directory.CreateDirectory("platforms");

			using(ZipArchive archive = ZipFile.OpenRead(zipfile))
			{
				foreach(ZipArchiveEntry entry in archive.Entries)
				{
					try
					{
						entry.ExtractToFile(System.IO.Path.Combine(Environment.CurrentDirectory + @".\", entry.FullName), true);
					}
					catch(IOException ex)
					{
						Clipboard.SetText(ex.ToString());

						MessageBox.Show(ex.ToString());
					}
				}
			}

			pbProgress.IsIndeterminate = false;
			statusText.Text = "Update Complete! Click the \"Finish\" button to close";
			File.Delete(TempDir + "libs.zip");
			bCancel.IsEnabled = true;
			bCancel.Content = "Finish";
			client = null;
		}
	}
}
