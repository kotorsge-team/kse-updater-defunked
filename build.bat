@echo off

set /p BUILDSEL="Select Build Configuration: (D - Debug // R - Release) "

if "%BUILDSEL%"=="D" (
	set BUILD="Debug"
) else if "%BUILDSEL%"=="d" (
	set BUILD="Debug"
) else if "%BUILDSEL%"=="R" (
	set BUILD="Release"
) else if "%BUILDSEL%"=="r" (
	set BUILD="Release"
) else (
	set BUILD="Release"
)

echo settings up environment
set CWD=%~dp0
set MSFW=C:\Windows\Microsoft.NET\Framework\v4.0.30319
set PATH=%PATH%;%MSFW%;
set CFLAGS=/p:Configuration=%BUILD%;AllowUnsafeBlocks=true /p:CLSCompliant=False
set LDFLAGS=/tv:4.0 /p:TargetFrameworkVersion=v4.5 /p:Platform="Any Cpu" /p:OutputPath="%CWD%\bin"

echo compiling application
msbuild kse-updater.sln %CFLAGS% %LDFLAGS%
if errorlevel 1 goto error
goto done

:error
echo An error occurred while compiling.. Check the output for errors
pause
exit

:done
echo Complete!
pause
