@echo off

echo settings up environment
set CWD=%~dp0

echo Cleaning up bin directory
rmdir /S /Q bin

cd "%CWD%\kse-updater"

echo Removing VS compiled garbage
rmdir /S /Q bin
rmdir /S /Q obj

echo Complete!
pause
